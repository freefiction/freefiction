FROM ruby:2.6.3
RUN apt -qq update && apt install -y build-essential postgresql-client libpq-dev nodejs

# For yarn since we use that.. (we really could do without it tho)
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt update -qq && apt install -y yarn dnsutils
COPY . /freefiction
ENV APP_HOME /freefiction
WORKDIR $APP_HOME
RUN yarn install
RUN bundle install
RUN rails assets:precompile

