# FreeFiction
FreeFiction is a free and open source platform for readers and writers. It is 
about freedom to write and not be restricted for the content you write. We won't 
impose rules on your freedom of creativity. It is entirely community driven.
Because of that, FreeFiction's development relies solely on the effort that 
everyone puts in their free time. We're not making money off of this.

FreeFiction aims to be useful for the people in the writing community by taking
in many elements that make the other platforms great. Such as FanFiction, 
Wattpad, GoodReads, etc. while also adding other unique features that users 
would find useful. Since this is currently in beta, many new ideas are still 
being considered. Right now we're focused on implementing the important features
that we think will make FreeFiction stand out from the rest of the crowd.

# Important features
Here are some important features that this site currently aims to have:

* Book cover creator for your convenience
* Storyboard creator for plotters 
* Accurate reading time predictions
* Dark mode, customise themes, fonts
* Export books to PDF
* ~~Communities for authors to collaborate~~ __Finished!__
* Twitter-like social media features. **TBA**

# Other features
Please see the [board](https://gitlab.com/freefiction/freefiction/boards) for 
more information.

# Contributions
Contributions are welcome. If you would like to help out, the project is 
currently in need of the following:

* A UX/UI person to make sure the website looks nice and modern and not from the
90s.
* Graphic artists to make FreeFiction's freedom fighting superhero mascot!
* Ruby on Rails developers for the backend
* Someone with a general passion for all things IT/CS/SE who just so happens to
also have an interest in writing or reading stories

## Developers 
If you're a developer, simply clone the repo `git clone https://gitlab.com/freefiction/freefiction.git`

### Linux / macOS
Since FreeFiction is written in Ruby, you'll need the Ruby 2.6.1 environment
setup. Please use [RVM](https://rvm.io/) to manage the ruby versions.
If not, then well, you'll figure out why eventually.

FreeFiction also relies on ActionText for the feature rich editor and requires
yarn to be installed. It's recommended that you get yarn from their
[official site](https://yarnpkg.com/en/docs/install), but you can also install
it from your distro's official repos. However, this is not tested. You may also
require `nodejs` to be installed too.

Ensure you have postgresql installed and setup. If not, google a guide for your distribution of choice.

Go to the project directory : `cd freefiction/`

Install the dependencies : `bundle install` and then `yarn install`.

Create the database and then migrate : `rails db:create` and then `rails db:migrate`

Then start the server : `rails s`

(If Yarn reports that it needs to be updated, then run : `yarn install` and repeat the last step.)

### Windows
`¯\_(ツ)_/¯` You're on your own! You could install Ruby through WSL and it might work.

### Troubleshooting
`ruby: error while loading shared libraries: libcrypt.so.1: cannot open shared object file: No such file or directory`
If you tried running the server but you ended up with this error, then you 
probably don't have the correct ruby binary selected. 

Ensure you have RVM installed.
Install Ruby 2.6.1 if you haven't already : `rvm install 'ruby-2.6.1'`.

Then simply tell RVM what version to use : `rvm use 2.6.1`.