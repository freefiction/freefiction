# frozen_string_literal: true

class CreateChapters < ActiveRecord::Migration[5.2]
  def change
    create_table :chapters do |t|
      t.references :book, foreign_key: true
      t.string :title
      t.string :content
      t.string :status
      t.timestamps
    end
  end
end
