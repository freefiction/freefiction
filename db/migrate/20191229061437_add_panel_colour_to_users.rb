class AddPanelColourToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :panel_colour, :string
  end
end
