class RemoveStatusFromChapters < ActiveRecord::Migration[6.0]
  def change
    remove_column :chapters, :status
  end
end
