# frozen_string_literal: true

class CreateBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :books do |t|
      t.belongs_to :user, foreign_key: true
      t.string :title
      t.string :description
      t.string :category
      t.string :rating
      t.string :language # not useful for now, but we'll leave it in just in case.
      t.string :status
      t.string :image
      t.string :author # should not be used. This data already exists in the user. Deprecate this after view is fixed.
      t.timestamps
    end
  end
end
