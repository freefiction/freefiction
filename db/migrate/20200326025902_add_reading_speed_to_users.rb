class AddReadingSpeedToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :reading_speed, :integer
  end
end
