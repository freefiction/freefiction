class CreateReactions < ActiveRecord::Migration[6.0]
  def change
    create_table :reactions do |t|
      t.belongs_to :user, foreign_key: true
      t.belongs_to :chapter, foreign_key: true
      t.string :emotion
    end
  end
end
