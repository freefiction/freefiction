class CreateCommunityMemberBans < ActiveRecord::Migration[6.0]
  def change
    create_table :community_member_bans do |t|
      t.belongs_to :community, foreign_key: true
      t.belongs_to :user, foreign_key: true
      t.string :reason
      t.timestamps
    end
  end
end
