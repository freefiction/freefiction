class CreateFeedback < ActiveRecord::Migration[6.0]
  def change
    create_table :feedbacks do |t|
      t.belongs_to :user, foreign_key: true
      t.belongs_to :chapter, foreign_key: true
      t.decimal :opening
      t.decimal :character_development
      t.decimal :story_logic
      t.decimal :dialogue
      t.decimal :originality
      t.decimal :conclusion
      t.decimal :grammar
      t.decimal :show_not_tell
    end
  end
end
