class CreateCommunities < ActiveRecord::Migration[6.0]
  def change
    create_table :communities do |t|
      t.belongs_to :user, foreign_key: true
      t.string :name
      t.string :description
      t.string :background
      t.string :color
      t.string :image
      t.string :status # Used for now to tell if it's an officially made community
      t.timestamps
    end
  end
end
