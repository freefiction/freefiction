class AddPublicToBooks < ActiveRecord::Migration[6.0]
  def change
    add_column :books, :public, :boolean
  end
end
