# frozen_string_literal: true

class HomeController < ApplicationController
  def index
    @books = Book.where(public: true)
    @communities = Community.all
  end

  def faq; end
end
