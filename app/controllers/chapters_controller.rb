# frozen_string_literal: true

class ChaptersController < ApplicationController
  def index
    @chapters = Chapter.all
  end

  def new
    @book = Book.find(params[:book_id])
    @chapter = @book.chapters.new
  end

  def create
    @book = Book.find(params[:book_id])
    if user_signed_in? && @book.user_id == current_user.id
      @chapter = @book.chapters.create(chapter_params)
      if @chapter.save
        redirect_to book_chapter_path(@book, @chapter)
      else
        logger.debug @chapter.errors.full_messages
        redirect_to books_path
      end
    else
      redirect_to 'index', notice: "You are not allowed to create chapters on #{@book.user.username}'s book!"
    end
  end

  def show
    @book = Book.find(params[:book_id])
    if @book.public || user_signed_in? && current_user.id == @book.user.id
      @chapter = Chapter.find(params[:id])
      @comments = @chapter.comments.all
      @commentable_type = "Chapter"
      @commentable_id = @chapter.id
      @reaction = Reaction.new
      @reactions = @chapter.reactions.all
      @feedback = Feedback.new
      @feedbacks = @chapter.feedbacks.all 
      @created_feedback = @chapter.feedbacks.find_by(user_id: current_user.id) if user_signed_in?
    else 
      render 'index', alert: 'The writer has made this chapter hidden.'
    end
  end

  def edit
    @chapter = Chapter.find(params[:id])
    if @chapter.book.user_id != current_user.id
      redirect_to books_path, alert: "You are not allowed to edit other people's books"
    end
  end

  def update
    @book = Book.find(params[:book_id])
    if user_signed_in? && @book.user_id == current_user.id
      @chapter = @book.chapters.find(params[:id])
      if @chapter.update(chapter_params)
        redirect_to book_chapter_path(@book, @chapter)
      else
        render 'edit'
        logger.debug @chapter.errors.full_messages
      end
    else
      redirect_to 'index', notice: "You are not allowed to update chapters on #{@book.user.username}'s book!"
    end
  end

  def destroy
    @book = Book.find(params[:book_id])
    if user_signed_in? && @book.user_id == current_user.id
      @chapter = @book.chapters.find(params[:id])
      @chapter.destroy
      redirect_to book_path(@book)
    else
      redirect_to 'index', notice: "You are not allowed to destroy chapters on #{@book.user.username}'s book!"
    end
  end

  private

  def chapter_params
    params.require(:chapter).permit(:book_id, :title, :content)
  end
end
