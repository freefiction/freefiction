# frozen_string_literal: true

class BooksController < ApplicationController
  def index
    #@books = Book.all
    @books = Book.search(params[:search]).where(public: true)
    filtering_params(params).each do |key, value|
      @books = @books.public_send(key, value) if value.present?
    end
    @book = Book.new
    @tags = Tag.order('taggings_count desc').limit(20)
  end

  def show
    @book = Book.find(params[:id])

    # refactor code below to be in a helper
    @words = 0
    @book.chapters.each do |c|
      arr = ActionController::Base.helpers.strip_tags(c.content.to_s).split
      @words += arr.count
    end
    if user_signed_in? && current_user.reading_speed != nil
      @minutes = @words / current_user.reading_speed
    else !user_signed_in? || user_signed_in? && current_user.reading_speed == nil
      @minutes = @words / 225
    end

    case @minutes
      when 0
        @reading_time = "less than a minute"
      when 1
        @reading_time = "#{@minutes} minute"
      when 2..60
        @reading_time = "#{@minutes} minutes"
      when 60..120
        @reading_time = "an hour"
      when 120..1440
        @reading_time = "#{@minutes / 60} hours"
    end
    if @minutes > 1440
      @reading_time = "a day"
    end

    # refactor above code to be in a helper

    if @book.public || user_signed_in? && current_user.id == @book.user.id
      @chapters = @book.chapters.all
      if @book.public
        impressionist(@book)
      end
    else
      render 'index', alert: 'The writer has made this chapter hidden.'
    end
  end

  def new
    @book = Book.new
  end

  def create
    if user_signed_in?
      # reminder: remove author from table as data is retrieved through association
      @book = Book.new(book_params.merge(user_id: current_user.id))
      unless @book.image.attached?
        @book.image.attach(io: File.open("#{Rails.root}/app/assets/images/default-cover.png"), filename: 'default-cover.png', content_type: 'image/jpg')
      end

      if @book.save
        redirect_to new_book_chapter_path(@book)
      else
        render 'index'
      end
    else
      render 'index', notice: "You must be signed in to create a book!"
    end
  end

  def edit
    @book = Book.find(params[:id])

    if @book.user_id != current_user.id
      redirect_to books_path, alert: "You are not allowed to edit other people's books"
    end
  end

  def update
    @book = Book.find(params[:id])
    if user_signed_in? && @book.user_id == current_user.id
      if @book.update(book_params.merge(user_id: current_user.id))
        redirect_to @book
      else
        render 'edit'
      end
    else
      redirect_to 'index', notice: 'You cannot update books that do not belong to you'
    end
  end

  def destroy
    @book = Book.find(params[:id])
    if user_signed_in? && @book.user_id == current_user.id
      @book.destroy
      redirect_to books_path
    else
      redirect_to 'index', "You are not allowed to destroy this book!"
    end
  end

  def favourite
    @book = Book.find(params[:id])
    type = params[:type]
    if type == "favourite"
      current_user.favourites << @book
      redirect_to book_path(@book)

    elsif type == "unfavourite"
      current_user.favourites.delete(@book)
      redirect_to book_path(@book)

    else
      # Type missing, nothing happens
      redirect_to :back, notice: 'Nothing happened.'
    end
  end

  private

  def book_params
    params.require(:book).permit(:title, :description, :rating, :status, :franchise, :image, :all_tags, :category, :public)
  end

  def filtering_params(params)
    params.slice(:status, :rating, :franchise, :tags, :category)
  end
end
