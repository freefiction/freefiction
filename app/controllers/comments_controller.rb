class CommentsController < ApplicationController
  before_action :authenticate_user!
  #before_action :set_commentable

  # POST /comments
  def create
    @comment = Comment.new(comment_params)
    @comment.user = current_user
    @commentable_type = @comment.commentable_type.constantize.find_by(id: @comment.commentable_id)
    case @comment.commentable_type
      # Ensure users can't leave a comment on a post unless they are members or they aren't banned.
    when "Post"
      @community = @commentable_type.community
      if !@community.community_member_bans.find_by(user_id: current_user.id)
        @comment.save
        respond_to do |format|
          format.html { redirect_to community_path(@commentable_type.community) }
        end
      else
        redirect_to '/', notice: 'You are not allowed to make this comment.'
      end
    when "Chapter"
      @comment.save
      respond_to do |format|
        format.html { redirect_to book_chapter_path(@commentable_type.book.id, @commentable_type.id) }
      end
    end
  end

  # DELETE /comments/1
  def destroy
    @comment = Comment.find(params[:id])
    # We need to find out where this comment came from
    @commentable_type = @comment.commentable_type.constantize.find_by(id: @comment.commentable_id)
    # this whole thing is very hacky... this needs refactoring if we want tighter moderation.
    case @comment.commentable_type
    when "Post"
      if @commentable_type.user_id == current_user.id || @comment.user_id == current_user.id
        @comment.destroy
        respond_to do |format|
          format.html { redirect_to community_path(@commentable_type.community) }
        end
      else
        redirect_to '/', notice: 'You are not allowed to delete this comment.'
      end
    when "Chapter"
      if @comment.user_id == current_user.id || @commentable_type.book.user_id = current_user.id
        @comment.destroy
        respond_to do |format|
          format.html { redirect_to book_chapter_path(@commentable_type.book.id, @commentable_type.id) }
        end
      else
        redirect_to '/', notice: 'You are not allowed to delete this comment.'
      end
    end
  end

  private

    #def set_commentable
    #  @commentable = Chapter.find(params[:chapter_id])
    #end

    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comment_params
      params.require(:comment).permit(:body, :commentable_id, :commentable_type)
    end
end
