class AdminController < ApplicationController
  before_action :authenticate_user!
  def show
    if current_user.role == 'Owner'
      @books = Book.all
      @communities = Community.all
      @users = User.all
    else
      redirect_to '/', alert: 'Unauthorised access!'
    end
  end
end
