# frozen_string_literal: true

class UsersController < ApplicationController
  def show
    @user = User.friendly.find(params[:id])
    if user_signed_in? && current_user.id == @user.id
      @books = @user.books
      @favourite_books = @user.favourite_books
    else user_signed_in? && current_user.id != @user.id || !user_signed_in?
      @books = @user.books.where(public: true)
      @favourite_books = @user.favourite_books.joins(:book).where(:books => {public: true})
    end
    

    @joined_communities = @user.community_members.all
    @owned_communities = Community.where(user_id: @user)
    @followers = @user.followers.all
    @following = @user.following.all
  end

  # PUT
  def follow
    @user = User.friendly.find(params[:id])
    type = params[:type]
    if type == "follow" && current_user != @user
      current_user.following << @user
      redirect_to user_path(@user)

    elsif type == "unfollow" && current_user != @user
      current_user.following.delete(@user)
      redirect_to user_path(@user)
    else
      redirect_to :back, notice: 'Invalid action!'
    end
  end
end
