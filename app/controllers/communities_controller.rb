class CommunitiesController < ApplicationController
  def index
    @communities = Community.all
  end
  def new
    @community = Community.new
  end
  def show
    @community = Community.friendly.find(params[:id])
    @post = @community.posts.new
    @posts = @community.posts.all
    # commentable type is defined here but the ID is in the view because the id
    # changes upon each loop through the posts and we need to associate the
    #comment with the correct post by it's ID.
    @commentable_type = "Post"
  end
  def create
    if user_signed_in?
      @community = Community.new(community_params.merge(user_id: current_user.id))
      unless @community.image.attached?
        @community.image.attach(io: File.open("#{Rails.root}/app/assets/images/community-default.png"), filename: 'community-default.png', content_type: 'image/jpg')
      end
      if @community.save
        redirect_to community_path(@community)
      else
        render 'index'
      end
    else
      render 'index', notice: "Please sign in to create a community"
    end
  end

  def edit
    @community = Community.friendly.find(params[:id])
    if @community.user_id != current_user.id
      redirect_to books_path, alert: "You are not the owner of this community!"
    end
  end

  def update
    @community = Community.friendly.find(params[:id])
    if user_signed_in? && @community.user_id == current_user.id
      if @community.update(community_params)
        redirect_to @community
      else
        render 'edit'
      end
    else
      redirect_to 'index', notice: 'You are not the owner of this community!'
    end
  end

  # Not be confused with membership. This is a members GET path.
  def members
    @community = Community.friendly.find(params[:id])
    @members = @community.community_members.all
    @banned_members = @community.community_member_bans.all
  end

  # This function takes in a PUT request with URL parameters matching by type.
  def membership
    @community = Community.friendly.find(params[:id])
    type = params[:type]
    if type == "join" && !@community.community_member_bans.find_by(user_id: current_user.id, community_id: @community.id)
      current_user.community_members.create(community_id: @community.id, user_id: current_user.id)
      redirect_to community_path(@community)

    elsif type == "leave"
      @community.community_members.find_by(user_id: current_user.id, community_id: @community.id).destroy
      redirect_to community_path(@community)

    elsif type == "kick" && @community.user_id == current_user.id
      @community.community_members.find_by(user_id: params[:who], community_id: @community.id).destroy
      redirect_to community_path(@community)

    elsif type == "ban" && @community.user_id == current_user.id
      # kick the user and then add them to ban list.
      @community.community_members.find_by(user_id: params[:who], community_id: @community.id).destroy
      @community.community_member_bans.create(community_id: @community.id, user_id: params[:who])
      redirect_to members_community_path(@community)

    elsif type == "unban" && @community.user_id == current_user.id
      @community.community_member_bans.find_by(community_id: @community.id, user_id: params[:who]).destroy
      redirect_to members_community_path(@community)

    else
      # Don't trust the user!
      redirect_to @community, notice: 'Unauthorised requests detected.'

    end
  end

  def destroy
    @community = Community.friendly.find(params[:id])
    if user_signed_in? && @community.user_id == current_user.id
      @community.destroy
      redirect_to communities_path
    else
      redirect_to 'index', notice: "You are not allowed to destroy someone's community!"
    end
  end

  private
  def community_params
    params.require(:community).permit(:name, :description, :image, :color, :background)
  end
end
