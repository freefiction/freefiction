class PostsController < ApplicationController
  def show
    @post = Post.find(params[:id])
    @commentable_type = "Post"
    @commentable_id = @post.id
  end

  def new
    @community = Community.friendly.find(params[:community_id])
    @post = @community.posts.new
  end

  def create
    @community = Community.friendly.find(params[:community_id])
    if user_signed_in? && !@community.community_member_bans.find_by(user_id: current_user.id)
      @post = @community.posts.create(post_params.merge(user_id: current_user.id))
      if @post.save
        redirect_to @community
      else
        logger.debug @post.errors.full_messages
        redirect_to @community
      end
    else
      redirect_to 'index', notice: "You are not authorised to write a post here!"
    end
  end

  def edit
    @community = Community.friendly.find(params[:id])
    @post = @community.posts.find(params[:community_id])
    if @post.user_id != current_user.id
      redirect_to books_path, alert: "You are not allowed to edit this post."
    end
  end

  def update
    @community = Community.friendly.find(params[:id])
    @post = @community.posts.find(params[:community_id])
    if user_signed_in? && @post.user_id == current_user.id
      if @post.update(post_params)
        redirect_to community_path(@community)
      else
        render 'edit'
        logger.debug @community.errors.full_messages
      end
    else
      redirect_to 'index', notice: "You are not allowed to update this post!"
    end
  end

  def destroy
    @community = Community.friendly.find(params[:id])
    @post = @community.posts.find(params[:community_id])
    if (user_signed_in? && @post.user_id == current_user.id) ||
      (user_signed_in? && @post.community.user_id == current_user.id)
      @post.destroy
      redirect_to community_path(@community)
    else
      redirect_to 'index', notice: "You are not allowed to destroy this post!"
    end
  end

  private
  def post_params
    params.require(:post).permit(:community_id, :content)
  end
end
