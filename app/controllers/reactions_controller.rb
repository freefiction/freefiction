class ReactionsController < ApplicationController
    before_action :authenticate_user!
    before_action :find_chapter
    def create
        @reaction = Reaction.new(reaction_params.merge(user_id: current_user.id, chapter_id: @chapter.id))
        emotions = ['happy', 'excited', 'sad', 'bored', 'angry', 'scared', 'love', 'laughter', 'confused', 'aroused']
        if Reaction.where(user_id: current_user.id, chapter_id: params[:chapter_id]).exists?
            redirect_to book_chapter_path(@chapter.book_id, @chapter.id), 
            alert: "Updating reactions doesn't work yet. Try deleting the reaction first and then making it again."
        else
            @reaction.save
            redirect_to book_chapter_path(@chapter.book_id, @chapter.id)
        end
    end

    def destroy 
        @reaction = Reaction.find(params[:id])
        if user_signed_in? && @reaction.user_id == current_user.id
          @chapter = Chapter.find(@reaction.chapter_id)
          @reaction.destroy
          redirect_to book_chapter_path(@chapter.book_id, @chapter.id)
        else
          redirect_to 'index', notice: "You are not allowed to destroy this reaction"
        end
    end
      
    
    private

    def reaction_params
        params.require(:reaction).permit(:emotion)
    end

    def find_chapter
        @chapter = Chapter.find(params[:chapter_id])
    end
end