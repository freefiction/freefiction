class FeedbacksController < ApplicationController
    before_action :authenticate_user!
    before_action :find_chapter
    def create
        @feedback = Feedback.new(feedback_params.merge(user_id: current_user.id, chapter_id: @chapter.id))
        if Feedback.where(user_id: current_user.id, chapter_id: params[:chapter_id]).exists?
            redirect_to book_chapter_path(@chapter.book_id, @chapter.id), 
            alert: "Updating your feedback doesn't work yet. Try deleting the feedback first and then making it again."
        else
            @feedback.save
            redirect_to book_chapter_path(@chapter.book_id, @chapter.id)
        end
    end

    def destroy 
        @feedback = Feedback.find(params[:id])
        if user_signed_in? && @feedback.user_id == current_user.id
          @chapter = Chapter.find(@feedback.chapter_id)
          @feedback.destroy
          redirect_to book_chapter_path(@chapter.book_id, @chapter.id)
        else
          redirect_to 'index', alert: "You are not allowed to destroy this feedback"
        end
    end
      
    
    private

    def feedback_params
        params.require(:feedback).permit(:opening, :character_development, :story_logic, :dialogue, :originality,
         :conclusion, :grammar, :show_not_tell)
    end

    def find_chapter
        @chapter = Chapter.find(params[:chapter_id])
    end
end