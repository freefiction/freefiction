json.array! @communities do |c|
  json.id c.id
  json.title c.name
  json.description truncate(strip_tags(c.description.to_s), length: 200)
  json.image url_for(c.image) if c.image.attached?
  json.background url_for(c.background) if c.background.attached?
  json.color c.color
  json.created_at c.created_at
  json.members c.community_members.count
end
