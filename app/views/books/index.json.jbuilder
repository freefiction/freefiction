json.array! @books do |b|
  json.id b.id
  json.title b.title
  json.description truncate(strip_tags(b.description.to_s), length: 200)
  json.image url_for(b.image)
  json.created_at b.created_at
  json.author do
    json.username b.user.username
    json.name "#{b.user.firstname}" + ' ' + "#{b.user.lastname}"
    json.urlSlug b.user.slug
  end
  json.tags b.tags.all
  json.views b.impressionist_count(:filter=>:ip_address)
  json.favourites b.favourited_by.count
  json.chapters b.chapters.count
end
