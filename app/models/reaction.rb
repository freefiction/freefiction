class Reaction < ApplicationRecord
    belongs_to :chapter
    belongs_to :user
    validates :emotion, presence: true
  end
  