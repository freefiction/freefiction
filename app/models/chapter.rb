# frozen_string_literal: true

class Chapter < ApplicationRecord
  include ActionText::Attachable
  belongs_to :book
  has_rich_text :content
  has_many :comments, as: :commentable
  has_many :reactions, dependent: :destroy
  has_many :feedbacks, dependent: :destroy

  def next
    Chapter.where("id > ? AND book_id = ?", id, book_id).first
  end
  def prev
    Chapter.where("id < ? AND book_id = ?", id, book_id).first
  end
end
