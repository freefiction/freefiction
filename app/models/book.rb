# frozen_string_literal: true

class Book < ApplicationRecord
  has_many :chapters, dependent: :destroy
  has_many :comments, through: :chapters, dependent: :destroy
  validates :title, presence: true
  validates :rating, presence: true
  validates :status, presence: true
  validates :category, presence: true
  has_one_attached :image
  validates :image, file_size: { less_than_or_equal_to: 5.megabytes },
                     file_content_type: { allow: ['image/jpeg', 'image/png'] }
  has_rich_text :description
  has_many :taggings, dependent: :destroy
  has_many :tags, through: :taggings, dependent: :destroy
  has_many :favourite_books
  has_many :favourited_by, through: :favourite_books, source: :user
  belongs_to :user
  has_many :reactions, through: :chapters
  has_many :feedbacks, through: :chapters
  is_impressionable :counter_cache => true, :column_name => :impressions_count, :unique => true
  scope :status, ->(status) { where status: status }
  scope :rating, ->(rating) { where rating: rating }
  scope :category, ->(category) { where category: category }

  # Finds books by tag, refers to models/tag.rb for the rest.
  scope :tags, -> (name){ joins(:tags).where(tags: {name: name}) }

  def self.search(search)
    if search
      book = Book.where("lower(title) LIKE ?", "%#{search.downcase}%")
      if book
        self.where(id: book)
      else
        Book.all
      end
    else
      Book.all
    end
  end

  def all_tags=(names)
    self.tags = names.split(',').map do |name|
      Tag.where(name: name.strip).first_or_create!
    end
  end

  def all_tags
    tags.map(&:name).join(', ')
  end
end
