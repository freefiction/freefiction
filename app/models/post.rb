class Post < ApplicationRecord
  belongs_to :community
  belongs_to :user
  has_rich_text :content
  validates :content, presence: true
  has_many :comments, as: :commentable
end
