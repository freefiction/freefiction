# frozen_string_literal: true

class Tag < ApplicationRecord
  has_many :taggings
  has_many :books, through: :taggings
  scope :tag, -> (name){ where(name: name) }
end
