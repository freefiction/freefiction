# frozen_string_literal: true

class User < ApplicationRecord

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  has_one_attached :image
  validates :image, file_size: { less_than_or_equal_to: 2.megabytes },
                     file_content_type: { allow: ['image/jpeg', 'image/png'] }
  has_many :books, dependent: :destroy
  has_many :chapters, through: :books
  has_many :community_members
  has_many :communities, through: :community_members
  has_many :favourite_books
  has_many :favourites, through: :favourite_books, source: :book
  has_many :comments
  has_many :community_member_bans
  has_many :posts, through: :communities
  has_many :reactions, dependent: :destroy
  has_many :feedbacks, dependent: :destroy

  # This piece of code is a bit complicated, but DRY and good practice.
  # This essentially creates a virtual table with the join table and messes with that.
  has_many :follows

  has_many :follower_relationships, foreign_key: :following_id, class_name: "Follow"
  has_many :followers, through: :follower_relationships, source: :follower

  has_many :following_relationships, foreign_key: :user_id, class_name: "Follow"
  has_many :following, through: :following_relationships, source: :following

  has_rich_text :biography
  validates_uniqueness_of :slug

  extend FriendlyId
  friendly_id :username, use: :slugged
  
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
end
