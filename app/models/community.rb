class Community < ApplicationRecord
  # Communities have many little things. We need to delete the right things if
  # for some reason the community owner wishes to delete the whole thing. :(
  belongs_to :user
  has_many :community_members, dependent: :destroy
  has_many :users, through: :community_members
  has_many :community_member_bans, dependent: :destroy
  has_many :posts, dependent: :destroy
  has_many :comments, through: :posts
  has_one_attached :image
  has_one_attached :background
  has_rich_text :description
  extend FriendlyId
  friendly_id :name, use: :slugged
  validates :background, file_size: { less_than_or_equal_to: 10.megabytes },
                     file_content_type: { allow: ['image/jpeg', 'image/png']}
  validates :image, file_size: { less_than_or_equal_to: 5.megabytes },
                     file_content_type: { allow: ['image/jpeg', 'image/png']}

end
