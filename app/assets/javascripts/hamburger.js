document.addEventListener('DOMContentLoaded', function () {
    let sidebar = document.getElementById("sidebar");
    let content = document.getElementById("content");
    let trigger = document.getElementById("hamburger-trigger");
    let icon = document.getElementById("hamburger-icon");
    let clicked = false;

    function hamburgerMenu() {
        if (clicked == false) {
            sidebar.style.width = "100vw";
            sidebar.style.display = "flex";
            content.style.width = "0vw";
            clicked = true;
            trigger.style.color = "#3ee860";
            icon.innerHTML = "close";
        } else {
            sidebar.style.width = "0vw";
            sidebar.style.display = "none";
            content.style.width = "100vw";
            trigger.style.color = "black";
            icon.innerHTML = "menu";
            clicked = false; 
        }

    }
    trigger.addEventListener("click", hamburgerMenu);
})