# frozen_string_literal: true

Rails.application.routes.draw do
  resources :comments
  resources :communities do
    put :membership, on: :member
    get :members, on: :member
    resources :posts
  end
  devise_for :users, controllers: {
    registrations: 'users/registrations'
  }
  # Do not put user routes above devise block or else bad things will happen.
  resources :users, :path => 'u', only: [:show] do
    put :follow, on: :member
    get 'relationships' => 'users#relationships'
    get 'following' => 'users#following'
  end
  #resources :follows
  get '/' => 'home#index'
  get 'faq' => 'home#faq'
  get 'admin' => 'admin#show'
  get 'about' => 'home#about'
  resources :books do
    put :favourite, on: :member
    resources :chapters
  end

  resources :reactions
  resources :feedbacks
  
  resources :chatrooms
  resources :messages
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
